const knex = require('../db.sqlite');

const User = knex.schema.createTableIfNotExists('users',
 (table) => {
  table.increments();
  table.string('username');
});

module.exports = User;
