const express = require('express');
const router = express.Router();
const User = require('../models/user');

router.get('/users', (req, res) => {
  User.find({}, 'username', (err, users) => {
    res.send(users);
  });
});

router.get('/users/:id', (req, res) => {
  User.findById(req.params.id,
    (err, users) => {
      res.send(users);
    });
});

router.put('/users/:id', (req, res) => {
  User.findById(req.params.id, (err, user) => {
    if (err) return res.status(404).send(err);

    user.username = req.body.username;
    user.save((err, updatedUser) => {
      if (err) return res.status(404).send(err);

      res.status(200).send('Updated!');
    })
  })
});

router.post('/users', (req, res) => {
  const userObj = req.body;
  const user = new User(userObj);
  user.save((err) => {
    if (err) {
      res.status(404).send(err);
    } else {
      res.status(200).send('Added!');
    }
  });
});

router.delete('/users/:id', (req, res) => {
  User.removeById(req.params.id, err => {
    if (err) {
      res.status(404).send(err);
    } else {
      res.status(200).send('Deleted!');
    }
  });
});

module.exports = router;
