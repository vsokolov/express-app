const express = require('express');
const router = express.Router();
const mongoConnected = require('../db');

router.get('/users', (req, res) => {
  mongoConnected.then(db => {
    db.collection('users')
      .find({}, { 'username': 1 }).toArray((err, users) => {
        res.send(users.map(v => v.username));
      });
  });
});

router.get('/users/:id', (req, res) => {
  mongoConnected.then(db => {
    db.collection('users')
      .findOne({ id: req.params.id }, (err, user) => {
        if (err) {
          res.send(err)
        } else {
          res.send(user);
        }
      });
  });
});

router.put('/users/:id', (req, res) => {
  const user = req.body;
  mongoConnected.then(db => {
    db.collection('users')
      .update(
        {id: req.params.id},
        user, err => {
        if (err) {
          res.status(404).send(err);
        } else {
          res.status(200).send('OK\n');
        }
      });
  });
});

router.post('/users', (req, res) => {
  const userObj = req.body;
  mongoConnected.then(db => {
    db.collection('users').insert(userObj, err => {
      if (err) {
        res.status(404).send(err);
      } else {
        res.status(200).send('Add!\n');
      }
    })
  });
});

router.delete('/users/:id', (req, res) => {
  mongoConnected.then(db => {
    db.collection('users')
      .removeById(req.params.id, err => {
      if (err) {
        res.status(404).send(err);
      } else {
        res.status(200).send('Delete!\n');
      }
    })
  })
});

module.exports = router;
