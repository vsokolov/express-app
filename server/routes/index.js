const express = require('express');
const router = express.Router();
const fs = require('fs');
const path = require('path');

const mongodbRoutes = require('./mongodb.routes');
const mongooseRoutes = require('./mongoose.routes')
const knexRoutes = require('./knex.routes');

router.get('/', (req, res) => {
  res.redirect('/home');
});

router.get('/home', (req, res) => {
  res.render('home', {
    message: "Welcome to home page!",
    name: "John",
    id: req.id
  })
});

router.get('/about', (req, res) => {
  res.render('about', {
    message: "About page"
  });
});

router.use('/api', mongodbRoutes);

router.use('/api2', mongooseRoutes);

router.use('/api3', knexRoutes);

router.get('*', (req, res) => {
  res.end("404!");
})

module.exports = router;
