const express = require('express');
const router = express.Router();
const knex = require('../db.sqlite');
const User = require('../models/user.sqlite');

// TODO: make middleware
const sendOk = (res, data) => {
  res.status(200).send({
    message: "OK",
    data
  });
}

// revalidator

router.get('/users', (req, res) => {
  User.then(
    () => knex('users').select('username')
  ).then(users => {
    json = {
      message: "OK",
      data: users
    }
    res.send(json);
  }).catch(err => res.status(404).send(err));
});

router.get('/users/:id', (req, res) => {
  User.then(() => {
    return knex
      .from('users')
      .where('id', req.params.id)
      .select('username');
  }).then((user) => {
    res.send(user);
  }).catch(err => res.status(404).send(err));
});

router.put('/users/:id', (req, res) => {
  User.then(() => {
    return knex
      .from('users')
      .where('id', req.params.id)
      .update({
        username: req.body.username
      });
    // .update('username', req.params.username)
  }).then(() => {
    res.status(200).send('OK\n');
  }).catch(err => res.status(404).send(err));
});

router.post('/users', (req, res) => {
  const userObj = req.body;
  User.then(() => {
    return knex.insert(userObj).into('users');
  }).then(() => {
    res.status(200).send('Add!\n');
  }).catch(err => res.status(404).send(err));
});

router.delete('/users/:id', (req, res) => {
  User.then(() => {
    const query = knex
      .from('users')
      .where('id', req.params.id)
      .del();
    // console.log(query.returning('*').toString());
    return query;
  }).then(() => {
    res.status(200).send('Deleted!\n');
  }).catch(err => res.status(404).send(err));
});

module.exports = router;
