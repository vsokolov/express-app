const knex = require('knex')({
  dialect: 'sqlite3',
  connection: {
    filename: './mydb.sqlite'
  },
  useNullAsDefault: true
});

module.exports = knex;
