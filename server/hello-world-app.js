require('dotenv').config();
const express = require('express');
const path = require('path');
const exhbs = require('express-handlebars');
const bodyParser = require('body-parser');
// const { requestMiddleware } = require('./request-middleware');
const requestMiddleware = require('./request-middleware').requestMiddleware;
const config = require('./config');
const mongoose = require('mongoose');
mongoose.connect(config.mongodb_url);

const app = express();
const port = process.env.PORT || 3000;
const routes = require('./routes');

app.engine('.hbs', exhbs({
  defaultLayout: 'main',
  extname: '.hbs',
  layoutsDir: path.join(__dirname, 'views/layouts'),
}))

app.set('view engine', '.hbs');
app.set('views', path.join(__dirname, 'views'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded());

app.use(requestMiddleware);

app.use(routes);

app.listen(port, (err) => {
  if (err) {
    return console.error(err);
  }
  console.log(`server is listening on http://localhost:${port}`);
});
